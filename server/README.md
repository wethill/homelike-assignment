# Homelike server for assignment

## Background information

###run
- edit /config/default.json
- provide mongodb connection path [mongodb://localhost:27017/assignment by default]
- npm install
- npm start

###current endpoints
- `/users`
- `/apartments`
- `/locations`
- `/graphql`
- `/graphiql`



##My solution

1. I added /countries endpoint that represent `countries` collection.
2. It is active on graphql endpoint
3. I included it to `locations.graphql.schema` schema as a representative of `country information
4. I added `limit` and `skip` functionalities
5. For `location : null` problem, I read some github issues and find out that a lot of developer have faced same issue. I saw that they fixed it by added a hook on their query. The problem is related to `_id` field and MongoDb `ObjectID`. When your documents have `_id` field with valid `Mongodb ObjectID format` **(12 bytes 24 characters in hex representation)**, *feathers-mongodb*  adapter tries to query with find by  **_id** as `String`,  but in database, MongoDB expect a find by `_id` field as `ObjectID` (Which is I assume).  So I decided to add a hook that convert **_id**, `String` to `ObjectID`. On the other hand, I changed **data** script to record **locations** with `_id: ObjectID("xxxxx")`. So I tried to prevent conflict betweeen adapter and database. By the way, **countries** documents `_id`'s  have not valid `Mongodb ObjectID format` so I could not convert them to `ObjectId`. Then it works. 


##What to do - for backend engineers
1. add new endpoint /countries which should represent the data from the `countries` collection
1. add `countries` to /graphql endpoint
1. add `country` to `locations.graphql.schema` as a representative of `country` information
1. add functionality to use `limit` and `skip` as a parameters to fetch data through `/graphql` endpoint
1. If you run the following query, location will always be `null`. Please figure out why this is happening.
After you found out how this happens, please describe the reason and how you found the issue. 
```query RootQuery($owner: String) {  
      apartments(owner: $owner) {  
        items {  
          location {  
            title  
          }  
        }  
      }  
    }
```  