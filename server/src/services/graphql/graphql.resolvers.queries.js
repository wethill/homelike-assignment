export default function(Query, Service, GetServiceName, FindServiceName) {
  Object.assign(Query, {
    [`${GetServiceName}`]: (root, args, context) => {
      return Service.find(Object.assign({}, context, { query: args })).then(result => result[0]);
    },
  });
Object.assign(Query, {
    [`${FindServiceName}`]: (root, args, context) => {
    
      // Add limit and skip functionalities
      ['limit', 'skip'].forEach(f=> { 
        if(args[f]) args[`$${f}`] = args[f];
        delete args[f];
      })

      const paginate = {
        default: 100,
        max: 200
      };
      return Service.find(Object.assign({}, context, { query: args, paginate})).then(result => {
        return { total: result.total, items: result.data };
      });
    },
  });
}
