export default [
    `
    type CountriesWithPagination {
      total: Int
      items: [Countries]
    }
    
    type Countries {
      _id: ID!
      title: String
    }
  `,
  ];
  