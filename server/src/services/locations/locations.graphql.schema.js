export default [
  `
  type LocationsWithPagination {
    total: Int
    items: [Locations]
  }
  
  type Locations {
    _id: ID!
    title: String
    country: Countries
  }
`,
];
