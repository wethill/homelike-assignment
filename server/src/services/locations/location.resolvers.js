
export default function (Countries) {

    const locationsResolvers = {
      Locations: {
        title: (location) => {
          return location.title;
        },
        country: (location) => {
          return Countries.find({ query: { _id: location.country }}).then(result=>{
            return result[0]
          });
        },
  
      }
    };
  
    return locationsResolvers;
  }
  