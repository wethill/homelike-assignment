const ObjectID = require('mongodb').ObjectID;

module.exports = {
  before: {
    all: [],
    find: [
      function (hook) {
        if(hook.params.query._id) {
          // Change string _id to ObjectID for query
          hook.params.query._id  = new ObjectID(hook.params.query._id);
        }
    
        return hook;
      }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
